# Conversão De Bases

Este projeto é um algoritmo com objetivo de converter números decimais, inteiros e positivos para outras bases, tais como binário, octal e hexadecimal. Esse programa foi desenvolvido pelos alunos do Curso de Engenharia da Computação da Universidade Tecnologica Federal do Paraná. O código faz parte da disciplina de Introdução a Engenharia e os autores estão [listados abaixo](#Autores).

A lógica por trás do funcionamento do programa tem como base o algoritmo mostrado na imagem abaixo.

<img src= https://user-content.gitlab-static.net/c875fe944bd50212981d0838779ca230468978bc/68747470733a2f2f63616c63756c617265636f6e7665727465722e636f6d2e62722f77702d636f6e74656e742f75706c6f6164732f323031382f30352f646563696d616c2d656d2d62696e25433325413172696f2e706e67 >

Resumindo, o programa realiza divisões sucessivas pela base (2 para binario, 8 para octal e 16 para hexadecimal) guardando os restos. Ao final, o numero resultante será dado pelo inverso dos restos recebidos. No caso do hexadeciamis, quando o resto for entre 10 e 15, o valor devolvido será entre A e F.


## Execução
Caso deseje executar o programa com o compilador em C na sua máquina, siga os passos a seguir:

Escolha um dos programas abaixo para a execução:

<https://code.visualstudio.com/download>
<https://www.codeblocks.org/downloads/binaries/>




Clone o código e execute o programa.


## Autores
Esse programa foi escrito por:

Avatar|Nome | Nickname| E-mail 
---|---|---|---
<img src="https://gitlab.com/uploads/-/system/user/avatar/8279875/avatar.png" width="100">|Anne Caroline Reolon Dalla Costa | @anne_caroliner | [annecarolinereolon@gmail.com](mailto:annecarolinereolon@gmail.com)

Avatar|Nome | Nickname| E-mail 
---|---|---|---
<img src="https://gitlab.com/uploads/-/system/user/avatar/8279824/avatar.png" width="100">|Rhomelio Anderson Sousa Albuquerque| @rhomelio | [rhomelio@alunos.utfpr.edu.br](mailto:rhomelio@alunos.utfpr.edu.br)

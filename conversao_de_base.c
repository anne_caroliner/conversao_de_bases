/*[Conversão de Base] - O código abaixo tem o objetivo de converter um número decimal em binário.

 Alunos do curso de Engenharia da Computação - 1º Período - UTFPR Campus Pato Branco - 2020-2. 
 Anne Caroline RA: 1882260
 Rhomelio Anderson RA: 2297957 */

#include <stdio.h>
#include <math.h>
#include <stdbool.h>
#include <ctype.h>


long int convert_base(int num1, int num2, long int num3, long int base);



int main(void)
{
    char opcao;
    do
    {
        
        int num, base, cont = 0;
        

        printf("\nInforme o numero que deseja converter(apenas valores inteiros): ");
        scanf("%d", &num);

        do
        {
            printf("\nSelecione uma opcao para realizar a conversao: \n");
            printf("1 - Binario\n");
            printf("2 - Octal\n");
            printf("3 - Hexadecimal\n");
            printf("Escolha uma opcao: ");
            scanf("%d", &base);
            

            if (base < 1 || base > 3)
            {
                
                printf("OPCAO INVALIDA! TENTATIVAS RESTANTES: %d\n\n", 3 - cont);
                cont++;

                
                return 1;
            }
        }while (base < 1 || base > 3);
        

        if (num == 1 || num == 0)
        {
            if (base != 3)
                printf("\nO novo numero eh: %d", num);

            
            else
                printf("\nO novo numero eh: 0x%d", num);

            return 0;
        }
        
        switch (base)
        {
            case 1:
            base = 2;
            break;

            case 2:
            base = 8;
            break;

            case 3:
            base = 16;
            break;
        }
        
        int k = 1;
        while (true)
        {
            if (pow(base, k) <= num)
                k++;
            else
                break;
        }

        printf("\n\nO novo numero eh: ");
        
        if (base == 16)
            printf("0x");

        for (long int i = k - 1; i >= 0; i--)
        {
            
            if (base == 16)
                printf("%c", (char)convert_base(num, k, i, base));

            else
                printf("%ld", convert_base(num, k, i, base));
        }

        printf("\n\nDeseja repetir a execucao do programa (S/s): ");
        setbuf(stdin, NULL);
        scanf("%c", &opcao);    
    }while(opcao == 's' || opcao == 'S');

    return 0;    
}









long int convert_base(int num1, int num2, long int num3, long int base)
{
    double resto;
    long int convert[num2];
    
    
    for (int i = 0; i < num2; i++)
    {
        
        resto = (double)num1 / base;
        resto -= (long int)resto;
        resto *= base;

        
        switch (base)
        {
            case 2:
            if (resto == 1)
            convert[i] = 1;

            else
            convert[i] = 0;
            num1 = (num1 - resto) / 2;

            break;

            case 8:
            convert[i] = resto;
            num1 = (num1 - resto) / 8;
            break;

            case 16:
            convert[i] = resto;
            num1 = (num1 - resto) / 16;
            break;
        }
    }
    
    switch (base)
    {
        case 16:
        if (convert[num3] > 9)
        {
            return convert[num3] + 55;
        }

        else
        {
            return convert[num3] + 48;

        }
        break;

        default:
            return convert[num3];
    }
}
